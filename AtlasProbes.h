/* ******************************************************************************************** */
/* Functions for OneWire probe DS18B20.                                                         */
/* ******************************************************************************************** */
// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)

#ifndef ATLASPROBES_H_
#define ATLASPROBES_H_        1

/*------------------------------------------------------------------------------------------------*/
/*                 Here the libraries call with all classes and their methods                     */
/* [WARNING]: All instantiate objects in this module allow the usage of all methods and           */
/* attributes declared in these libraries on the condition that they are called here.             */
/*------------------------------------------------------------------------------------------------*/
#include      <Wire.h>        // Call of libraries or classes with their methods to make calculus or other actions.
#include      <Arduino.h>     // Object Serial declared in module

/*------------------------------------------------------------------------------------------------*/
/*                                   C++ Preprocessor directives                                  */
/* [NOTICE]: Shared preprocessor directives which are used by the main file or in each module     */
/* where they are declared. As modules are called by the main file, the directive is active in    */
/* the main file and in the module himself.                                                       */
/* Each module needs to activate or inhibit directive in the header file obviously.               */
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                                       SYSTEM CONSTANTS                                         */
/*------------------------------------------------------------------------------------------------*/
#if !defined Null
  #define       Null              '\0'
#endif
#define       Cmd_Information     'i'
#define       Cmd_Single_Read     'R'
#define       Cmd_Status          "Status"
#define       Cal_DO_Atmos        "Cal"
#define       Cal_DO_Zero         "Cal,0"
#define       Cal_EC_Dry          "Cal,dry"
#define       Cal_MySolution      "Cal,"
#define       Std_1415us          "Cal,1413"
#define       Std_84us            "Cal,84"
#define       Cmd_Cal_mid         "Cal,mid,"
#define       Cmd_Cal_low         "Cal,low,"
#define       Cmd_Cal_high        "Cal,high,"
#define       Cmd_CalClear        "Cal,clear"
#define       Cmd_NbrPtsCal       "Cal,?"
#define       Cmd_SLEEP           "Sleep"
#define       Chg_add_i2c         "I2C,"
#define       Compensate_temp     "T,"
#define       Request_CompT       "T,?"
#define       AddressEC_Probe     100               // same thing as uint8_t AddressEC_Probe = 0x64;

/*------------------------------------------------------------------------------------------------*/
/*                   General Purpose Input Output (GPIO) functions / Pins mapping                 */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/*                                 Flags as positions in registers                                */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/*               Predefined data types (Data Types and Predefined Structures)                     */
/* Definition types using keyword typedef and often in usage with typedef enum or typedef struct. */
/*------------------------------------------------------------------------------------------------*/
typedef union flottant {
  float value;
  uint8_t byte_value[4];      // représentation en little-endian (https://fr.wiktionary.org/wiki/little-endian)
} MyFloat_t;

typedef enum DelayTimes : uint16_t {
  delayMeasure = 600,         // wait a delaytime to allow the probe sending measure
  delayInformation = 300,
  delayCalibration = 600,
  delayTempCompensation = 300,
  delatSetProbeType = 300,
  delayStatus = 300,
  delayShowName = 300
} DelayTimes_t;

typedef enum I2CProbesAddresses : uint8_t {
  DissolvedOxygen_Add = 0x61,           // address_DO_EZO (97)
  ORP_Add = 0x62,                       // address_ORP_EZO (98)
  pH_Add = 0x63,                        // address_pH_EZO (99)
  Conductivity_Add = 0x64,              // address_EC_EZO (100 same thing as uint8_t = 0x64)
  RTD_Add = 0x66,                       // address_RTD_EZO (102)
  VEML7700_Add = 0x10,                  // address VEML7700 (16)
  NoI2C_Add = 0x00
} I2CProbesAddresses_t;

/*------------------------------------------------------------------------------------------------*/
/*                          Function prototypes or functions interface                            */
/*------------------------------------------------------------------------------------------------*/
void I2C_Initialization(void);
void separatorAtlasProbes(uint8_t, char);
char *EZO_CommandAndAnswer(char *, uint8_t, uint16_t);
char *EZO_CommandAndAnswer2(String, uint8_t, uint16_t);
char *ConvertStringToArray(String);
void PrintOnSerialMonitor(char *, boolean);

#endif /* ATLASPROBES_H_ */


/* ######################################################################################################## */
// END of file
