// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)
// NOTE:
//
/*----------------------------------------------------------------------------------*/
/*                 Functions call to be used here in this module                    */
/*----------------------------------------------------------------------------------*/
#include      "AtlasProbes.h"

/*----------------------------------------------------------------------------------*/
/*                                Class instances                                   */
/* [WARNING]: Objects declared here have to target a constructor of a library       */
/* called by this module. Constructor with public methods and private attributes.   */
/*----------------------------------------------------------------------------------*/
#define		MyI2CDevice		Wire


/*----------------------------------------------------------------------------------*/
/*            Common variables shared by the functions of this module               */
/* [WARNING]: By default you should prefer to declare local variables implemented   */
/* in functions when there is enough memory space to avoid shared variables.        */
/*----------------------------------------------------------------------------------*/
char          sent_by_probe[64];        // incoming data from EZO module, page 40 version 6.4
char          EZO_SentCmd[32];          // Command sent to the EZO module
uint8_t       NbrBytesFromEZO;
int           in_char = 0;
uint8_t       IndexArray;
char          *EZOPtr;


/*----------------------------------------------------------------------------------*/
/*                              Pins mapping                                        */
/*----------------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------------*/
/*                          FUNCTIONS of this module                                */
/*----------------------------------------------------------------------------------*/
/************************************************************************************************/
/* Initialization of I2C bus                                                                    */
/************************************************************************************************/
void I2C_Initialization() {
  MyI2CDevice.begin();
}
/************************************************************************************************/
/* Text divider                                                                                 */
/************************************************************************************************/
void separatorAtlasProbes(uint8_t nbr_carac, char caract) {
  uint8_t k;
  for (k = 0; k < nbr_carac; k++) Serial.print(caract);
  Serial.println();
}
/************************************************************************************************/
/* Function to send command to an EZO module and to retrieve an answer from it.                 */
/* The returned type is an char pointer.                                                        */
/************************************************************************************************/
char *EZO_CommandAndAnswer(char *PtrCde, uint8_t Address, uint16_t delaytime) {
  uint8_t m = 0;
  Serial.print(F("Content of pointer: ")); Serial.println(delaytime);
  memset(EZO_SentCmd, Null, sizeof(EZO_SentCmd));   // char EZO_SentCmd[32];
  do {
    EZO_SentCmd[m++] = *(PtrCde++);
  } while (*PtrCde != Null);
  Serial.print(F("Command sent: "));
  Serial.println(EZO_SentCmd);
  MyI2CDevice.beginTransmission(Address);
  MyI2CDevice.write(EZO_SentCmd);
  MyI2CDevice.endTransmission();
  delay(delaytime);
  MyI2CDevice.requestFrom(Address, 20, 1); // uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity_max, uint8_t sendStop)
  in_char = MyI2CDevice.read();            // first byte read
  switch (in_char) {
    case 1:
      Serial.print("Success => ");
      break;
    case 2:
      Serial.println("Failed");
      break;
    case 254:
      Serial.println("Pending");
      break;
    case 255:
      Serial.println("No Data");
      break;
  }
  memset(sent_by_probe, Null, sizeof(sent_by_probe));     // char sent_by_probe[64];
  IndexArray = 0;
  while (MyI2CDevice.available()) {
    in_char = MyI2CDevice.read();                        // int TwoWire::read(void)
    sent_by_probe[IndexArray++] = (char)in_char;
    if (in_char == Null) {
      IndexArray = 0;
      MyI2CDevice.endTransmission();
      break;
    }
  }
  return &sent_by_probe[0];
}
/************************************************************************************************/
/* Function to send command to an EZO module and to retrieve an answer from it.                 */
/* The returned type is an char pointer.                                                        */
/************************************************************************************************/
char *EZO_CommandAndAnswer2(String StringToConvert, uint8_t Address, uint16_t delaytime) {
  int ResponseCode;
  memset(EZO_SentCmd, Null, sizeof(EZO_SentCmd));         // char EZO_SentCmd[32];
  StringToConvert.toCharArray(EZO_SentCmd, StringToConvert.length() + 1);
  Serial.print(F("\u2731 Command sent to EZO device: "));
  Serial.println(EZO_SentCmd);
  MyI2CDevice.beginTransmission(Address);
  MyI2CDevice.write(EZO_SentCmd);
  MyI2CDevice.endTransmission();
  delay(delaytime);
  MyI2CDevice.requestFrom(Address, 20, 1);                // uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity_max, uint8_t sendStop)
  ResponseCode = MyI2CDevice.read();                      // first byte read
  memset(sent_by_probe, Null, sizeof(sent_by_probe));     // char sent_by_probe[64];
  IndexArray = 0;
  while (MyI2CDevice.available()) {
    in_char = MyI2CDevice.read();                         // int TwoWire::read(void)
    sent_by_probe[IndexArray++] = (char)in_char;
    if (in_char == Null) {
      IndexArray = 0;
      MyI2CDevice.endTransmission();
      break;
    }
  }
  Serial.print(F("\u2714 Response code from EZO module: "));
  switch (ResponseCode) {                                 // need to be after I2C buffer reading
    case 1:
      Serial.println("Success");
      break;
    case 2:
      Serial.println("Failed");
      break;
    case 254:
      Serial.println("Pending");
      break;
    case 255:
      Serial.println("No Data");
      break;
  }
  return &sent_by_probe[0];
}
/************************************************************************************************/
/* Function to convert a command as a String type into an array of char. The returned type      */
/* is a public char pointer or the address of the first location of a public array.             */
/************************************************************************************************/
char *ConvertStringToArray(String StringToConvert) {
  memset(EZO_SentCmd, Null, sizeof(EZO_SentCmd));
  StringToConvert.toCharArray(EZO_SentCmd, StringToConvert.length() + 1);
  Serial.print(F("Command sent to EZO device: "));
  Serial.println(EZO_SentCmd);
  return &EZO_SentCmd[0];
}
/************************************************************************************************/
/* Function to write the content of an area pointed by a char pointer until '\0' (Null) char is */
/* encountered.                                                                                 */
/************************************************************************************************/
void PrintOnSerialMonitor(char *LclPtr, boolean CarriageReturn) {
  do {
    Serial.print(*(LclPtr++));
  } while (*LclPtr != Null);
  if (CarriageReturn == true) Serial.println();
}









/* ######################################################################################################## */
// END of file
