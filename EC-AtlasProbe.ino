/*----------------------------------------------------------------------------------*/
/*                 Functions call and used only in the main module                  */
/*----------------------------------------------------------------------------------*/
#include      "AtlasProbes.h"           // the library you can complete       

/*------------------------------ Instantiate objects ------------------------------*/

/* ----------- Declaration of variables, uint8_t, bytes, floats, arrays ---------- */   
char BytesFromEZO[32];
char *LclPtrChar;
char *CdePtr;
char MyCommand[32];

/* ------------------- INITIALIZATION ------------------- */
void setup() {                // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial);
  I2C_Initialization();
  separatorAtlasProbes(100, '#');
  Serial.println(F("- Information about device and firmware -"));
  LclPtrChar = EZO_CommandAndAnswer2((String)Cmd_Information, Conductivity_Add, delayInformation);
  Serial.print(F("\u2731 Answer from probe with information command ('i'): "));
  PrintOnSerialMonitor(LclPtrChar, true);
  separatorAtlasProbes(100, '#');
}

/* ------------------- Infinite loop  ------------------- */
void loop() {           // put your main code here, to run repeatedly:
  separatorAtlasProbes(100, '-');
  Serial.println("Infinity loop for conductivity probe");
  separatorAtlasProbes(100, '-');
  Serial.println(F("- Measure from conductiy probe -"));
  do {
    LclPtrChar = EZO_CommandAndAnswer2((String)Cmd_Single_Read, Conductivity_Add, delayMeasure);
    Serial.print(F("Conductivity measure: "));
    PrintOnSerialMonitor(LclPtrChar, false);
    Serial.println(F(" \u03BCS"));
    separatorAtlasProbes(40, '-');
    delay(2000);
  } while (1);

}






/* END of file */
